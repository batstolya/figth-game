import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let hitA = getHitPower(attacker);
  let hitB = getBlockPower(defender);
  if (hitB > hitA) {
      return 0;
  } 
  return hitA - hitB;
}

export function getHitPower(fighter) {
  // return hit power
  let power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  let power = defense * dodgeChance;
  return power;
}
